var express = require('express');
var app = express();
var bodyParser = require('body-parser')

app.use(express.json());
app.use(bodyParser.urlencoded({
    extended: true
  }));

app.set('view engine', 'ejs');

app.use(express.static(__dirname + '/public'));

app.locals.app = app
app.locals.init = true
app.locals.isLoggedIn = false
app.locals.credsCorrect = true

app.get('/', function(req, res) {
    res.render('pages/index');
});

app.get('/play', function(req, res) {
    if(app.locals.isLoggedIn){
        res.render('pages/game')
    } else {
        console.log("You need to login first")
        app.locals.init = false
        res.redirect('/')
    }
    
});

app.get('/login', function(req, res) {
    app.locals.credsCorrect = true
    res.render('pages/login');
});

app.post('/login', function(req, res) {
    const username = req.body.username
    const password = req.body.password

    if(username === 'pro_gamer' && password === 'palingpro') {
        app.locals.isLoggedIn = true
        res.redirect('/')
    }
    else {
        console.log("Wrong credentials!!!");
        app.locals.credsCorrect = false
        res.render("pages/login")
    }
    
});

app.listen(8080);
console.log('Server running in port 8080');